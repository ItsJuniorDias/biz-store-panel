import Constant from '../models/constant';

class ConstantController {
  async index(req, res) {
    const constant = await Constant.findAll();

    return res.json(constant);
  }

  async store(req, res) {
    const { name } = req.body;

    const constant = await Constant.create({ name });

    return res.json(constant);
  }
}

export default new ConstantController();
