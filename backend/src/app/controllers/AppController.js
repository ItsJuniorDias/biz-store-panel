import App from '../models/app';
class AppController {
  async index(req, res) {
    const apps = await App.findAll();

    return res.json(apps);
  }

  async show(req, res) {
    const app = await App.findByPk(req.params.id);

    return res.json(app);
  }

  async store(req, res) {
    const { name, value } = req.body;

    const app = await App.create({ name, value });

    return res.json(app);
  }

  async update(req, res) {
    const id = req.params.id;

    App.update(req.body, {
      where: { id: id },
    });

    return res.json({ message: 'Updated App successfully' });
  }

  async delete(req, res) {
    const id = req.params.id;

    App.destroy({
      where: { id: id },
    });

    return res.json({ message: 'Delete App successfully' });
  }
}

export default new AppController();
