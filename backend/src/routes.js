import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';

import ConstantController from './app/controllers/ConstantController';
import AppController from './app/controllers/AppController';

const routes = new Router();

routes.post('/apps', AppController.store);
routes.get('/apps', AppController.index);
routes.put('/apps/:id', AppController.update);
routes.get('/apps/:id', AppController.show);
routes.delete('/apps/:id', AppController.delete);

routes.post('/constants', ConstantController.store);
routes.get('/constants', ConstantController.index);

export default routes;
