import Sequelize from 'sequelize';

import Constant from '../app/models/constant';
import App from '../app/models/app';

import databaseConfig from '../config/database';

const models = [Constant, App];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models
      .map((model) => model.init(this.connection))
      .map(
        (model) => model.associate && model.associate(this.connection.models)
      );
  }
}

export default new Database();
