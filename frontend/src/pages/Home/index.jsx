/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Link, useHistory, useParams } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles.css";
import api from "../../services/api";

const initialFormState = { id: null, name: "", value: "" };

const Home = (props) => {
  const [constants, setConstants] = useState([]);
  const [apps, setApps] = useState([]);

  const [editing, setEditing] = useState(false);
  const [currentUser, setCurrentUser] = useState(initialFormState);

  const { register, handleSubmit, errors } = useForm();

  const [values, setValues] = useState([]);

  const history = useHistory();

  const editApp = (id) => {
    history.push(`/dashboard/${id}`);
  };

  const onSubmit = async (data) => {
    console.log(data);
    const { name } = data;

    const jsonb = JSON.stringify(data);

    const newData = {
      name,
      value: jsonb,
    };

    await api.post("apps", newData);
    alert("App criado");

    console.log(jsonb);
  };

  useEffect(() => {
    api.get("constants").then((response) => {
      setConstants(response.data);
    });
  }, []);

  useEffect(() => {
    api.get("apps").then((response) => {
      setApps(response.data);
    });
  }, []);

  const handleRemove = async (id) => {
    setApps(apps.filter((app) => app.id !== id));

    const response = await api.delete(`apps/${id}`);

    console.log(response);
    return response;
  };

  return (
    <>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/" className="navbar-brand">
          Biz Store
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/dashboard"} className="nav-link">
              Dashboard
            </Link>
          </li>
        </div>
      </nav>

      <div className="App">
        <form onSubmit={handleSubmit(onSubmit)}>
          <h1>Welcome, Junior</h1>

          <span>Nome do App</span>
          <input name="name" ref={register} />

          {constants.map((constant) => (
            <div>
              <label>{constant.name}</label>
              <input
                name={constant.name}
                ref={register}
                value={values[constant.name]}
              />
            </div>
          ))}

          <input className="btn btn-success" type="submit" />
        </form>

        <div>
          <h1>Apps cadastrados</h1>
          <div className="containerApp">
            {apps.map((app, id) => (
              <div>
                <table>
                  <thead>
                    <th>Name</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{app.name}</td>
                      <td>
                        <button
                          onClick={() => editApp(app.id)}
                          className="btn btn-warning"
                        >
                          Edit
                        </button>
                        <button
                          onClick={() => handleRemove(app.id)}
                          className="btn btn-danger"
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
