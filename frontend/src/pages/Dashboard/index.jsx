import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import api from "../../services/api";

function Dashboard(props) {
  const { id } = useParams();
  const [apps, setApps] = useState([]);

  const { register, handleSubmit, errors } = useForm();

  useEffect(() => {
    const response = api.get(`apps/${id}`).then((response) => {
      setApps(response.data);
    });
    console.log(response);
  }, [id]);

  return (
    <>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/" className="navbar-brand">
          Biz Store
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/dashboard"} className="nav-link">
              Dashboard
            </Link>
          </li>
        </div>
      </nav>

      <form onSubmit={handleSubmit()}>
        <h1>Update App</h1>

        <div>
          <label>ANDROID_VERSION</label>
          <input name="Android" ref={register} />
        </div>

        <button className="btn btn-success" type="submit">
          Update App
        </button>
      </form>
    </>
  );
}

export default Dashboard;
