import React, { useState } from "react";
import ImageUploader from "react-images-upload";

const Dropzone = (props) => {
  const [pictures, setPictures] = useState([]);

  const onDrop = (pictureFiles) => {
    setPictures([...pictures, pictureFiles]);
  }

  console.log(pictures)

  return (
    <ImageUploader
      {...props}
      withIcon={true}
      buttonText="Choose images"
      onChange={onDrop}
      imgExtension={[".jpg", ".gif", ".png", ".gif"]}
      maxFileSize={5242880}
      withPreview={true}
    />
  );
}



export default Dropzone;