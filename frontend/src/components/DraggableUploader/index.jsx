
import React, { useState, useRef } from "react";

import { AnchorButton, Intent, ProgressBar } from "@blueprintjs/core";
import './styles.scss';
import lodash from "lodash";

import { Icon } from "react-icons-kit";
import { remove } from 'react-icons-kit/fa/remove';

const DraggableUploader = () => {
  const [loadedFiles, setLoadedFiles] = useState([]);

  const fileInput = useRef();

  const onFileLoad = (e) => {
    const file = e.currentTarget.files[0];

    const fileReader = new FileReader();
    fileReader.onload = () => {
      console.log("IMAGE LOADED: ", fileReader.result);
      const file = {
        data: fileReader.result,
        isUploading: false
      }
      //Add file
      addLoadedFile(file);
    }

    fileReader.onabort = () => {
      alert("Reading Aborted");
    }

    fileReader.onerror = () => {
      alert("Reading ERROR!");
    }

    fileReader.readAsDataURL(file);
  }

  const addLoadedFile = (file) => {
    setLoadedFiles([...loadedFiles, file])
  }

  const handleRemoveFile = (data) => {
    setLoadedFiles(lodash.filter(loadedFiles => loadedFiles.data !== data));
  }

  // const removeAllLoadedFile = () => {
  //   setLoadedFiles({ loadedFiles: [] });
  // }


  const handleChange = e => {
    const loadedFiles = e.target;

    setLoadedFiles(prevState => ({
      ...prevState,
      loadedFiles
    }));
  };

  const onUpload = (event) => {


    loadedFiles.map((file) => {

      const newFile = handleChange(file, {
        ...file,
        isUploading: true
      });

      //Simulate a REAL WEB SERVER DOING IMAGE UPLOADING (3seconds)
      setTimeout(() => {
        //Get it back to it's original State
        this.handleChange(newFile, {
          ...newFile,
          isUploading: false
        });
      }, 3000);

    });
  }


  return (
    <div
      className="inner-container"
      style={{
        display: "flex",
        flexDirection: "column"
      }}>
      <div className="sub-header">Drag an Image</div>
      <div className="draggable-container">
        <input
          type="file"
          id="file-browser-input"
          name="file-browser-input"
          ref={fileInput}
          onDragOver={(e) => {
            e.preventDefault();
            e.stopPropagation();
          }}
          onDrop={onFileLoad}
          onChange={onFileLoad} />
        <div className="files-preview-container ip-scrollbar">
          {loadedFiles.map((file, idx) => {
            return <div className="file" key={idx}>
              <img src={file.data} />
              <div className="container">
                <span className="progress-bar">
                  {file.isUploading && <ProgressBar />}
                </span>
                <span className="remove-btn" onClick={handleRemoveFile}>
                  <Icon icon={remove} size={19} />
                </span>
              </div>
            </div>
          })}
        </div>
        <div className="helper-text">Drag and Drop Images Here</div>
        <div className="file-browser-container">
          <AnchorButton
            text="Browse"
            intent={Intent.PRIMARY}
            minimal={true}
            onClick={fileInput} />
        </div>
      </div>
      <AnchorButton
        text="Upload"
        intent={Intent.SUCCESS}
        onClick={onUpload} />
    </div>
  );

}

export default DraggableUploader;